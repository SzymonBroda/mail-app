import imaplib, email, smtplib, os
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from myUi import *
from sys import exit, argv


class MailApp(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.signed_in = False
        self.server_smtp = None
        self.server_imap = None
        self.img = None
        self.sender = None
        self.password = None
        self.downloaded = False
        self.num = 1
        self.ui.status.setText("Welcome to MailApp")

        self.ui.signIn.clicked.connect(self.sign_in)
        self.ui.LogOut.clicked.connect(self.log_out)
        self.ui.Send.clicked.connect(self.send)
        self.ui.attachImage.clicked.connect(self.attach_img)
        self.ui.receive.clicked.connect(self.receive)
        self.show()

    #pobieranie wybranych wiadomoscie
    def get_msgs(self, id_list):
        msgs = []
        for d in id_list[0].split():
            result, data = self.server_imap.fetch(d, ('RFC822'))
            msgs.append(data)
        return msgs
    #odczytywanie wiadomości
    def receive(self):
        if self.signed_in:
            try:
                self.server_imap = imaplib.IMAP4_SSL('imap.gmail.com')
                self.server_imap.login(self.sender, self.password)
                self.server_imap.select('INBOX')
                id_list = self.search('SUBJECT', self.ui.search.text())
                msgs = self.get_msgs(id_list)
                last_msg = msgs[-(self.num % len(msgs))]
                raw = email.message_from_bytes(last_msg[0][1])
                self.get_info(raw)
                self.download_attachments(raw)
                self.ui.body.setText(self.get_body(raw).decode("utf-8"))
                if self.downloaded:
                    self.ui.status.setText("Mail received. Attachment downloaded")
                    self.downloaded = False
                else:
                    self.ui.status.setText("Mail received")
                self.num += 1
            except:
                self.ui.status.setText("Error in mail receiving. Not such email found ")

        else:
            self.ui.status.setText("Error in mail receiving. Sign in")
    #przeszukiwanie skrzynki
    def search(self, key, value):
        result, data = self.server_imap.search(None, key, '"{}"'.format(value))
        return data
    #załączanie obrazka
    def attach_img(self):
        try:
            with open(self.ui.img.text(), 'rb') as myfile:
                self.img = MIMEImage(myfile.read())
            self.img.add_header('Content-Disposition', 'attachment', filename=''.join(("received-", self.ui.img.text())))
            self.ui.status.setText("File attached")
        except:
            self.ui.status.setText("File not attached. Try again")
    #wysyałenia maila
    def send(self):
        message = MIMEMultipart()
        message["Subject"] = self.ui.Title.text()
        message["From"] = self.sender
        message["To"] = self.ui.receiver.text()

        text = MIMEText(self.ui.body.toPlainText(), "plain")
        message.attach(text)
        if self.img != None:
            message.attach(self.img)
        if self.signed_in:
            try:
                self.server_smtp.sendmail(self.sender, self.ui.receiver.text(), message.as_string())
                self.ui.status.setText("Email sent")
                self.img = None
            except:
                self.ui.status.setText("Error in message sending")
        else:
            self.ui.status.setText("Error in message sending. Sign in")
    #logowanie się do poczty
    def sign_in(self):
        self.sender = self.ui.sender.text()
        self.password = self.ui.password.text()
        self.server_smtp = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        try:
            self.server_smtp.login(self.sender, self.password)
            self.ui.status.setText("Successfully signed in")
            self.signed_in = True
        except:
            self.ui.status.setText("Unsuccessfully signed in. Try again")
        self.ui.sender.setText("")
        self.ui.password.setText("")
    #wylogowywanie się
    def log_out(self):
        if self.server_smtp != None:
            self.ui.status.setText("Logged out")
            self.signed_in = False
            self.server_smtp.quit()
            self.server_smtp = None
    #informacje o mailu
    def get_info(self,msg):
        if msg["From"] is None:
            msg["From"] = ""
        if msg["Subject"] is None:
            msg["Subject"] = ""
        self.ui.receiver.setText(msg["From"])
        self.ui.Title.setText(msg["Subject"])
    #odczytywanie tekstu
    def get_body(self, msg):
        if msg.is_multipart():
            return self.get_body(msg.get_payload(0))
        else:
            return msg.get_payload(None, True)
    #pobieranie załączników
    def download_attachments(self, msg):
        for part in msg.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            img = part.get_filename()
            if bool(img):
                with open(img, 'wb') as file:
                    file.write(part.get_payload(decode=True))
                self.downloaded = True


def run():
    app = QtWidgets.QApplication(argv)
    MainWindow = MailApp()
    exit(app.exec_())

run()

