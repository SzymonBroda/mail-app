# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'myUi.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(525, 516)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.sender = QtWidgets.QLineEdit(self.centralwidget)
        self.sender.setGeometry(QtCore.QRect(350, 10, 161, 20))
        self.sender.setObjectName("sender")
        self.password = QtWidgets.QLineEdit(self.centralwidget)
        self.password.setGeometry(QtCore.QRect(350, 40, 161, 20))
        self.password.setObjectName("password")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(310, 10, 47, 13))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(290, 40, 47, 13))
        self.label_2.setObjectName("label_2")
        self.attachImage = QtWidgets.QPushButton(self.centralwidget)
        self.attachImage.setGeometry(QtCore.QRect(10, 400, 101, 23))
        self.attachImage.setObjectName("attachImage")
        self.img = QtWidgets.QLineEdit(self.centralwidget)
        self.img.setGeometry(QtCore.QRect(120, 401, 113, 21))
        self.img.setObjectName("img")
        self.body = QtWidgets.QTextEdit(self.centralwidget)
        self.body.setGeometry(QtCore.QRect(10, 190, 501, 201))
        self.body.setObjectName("body")
        self.receiver = QtWidgets.QLineEdit(self.centralwidget)
        self.receiver.setGeometry(QtCore.QRect(150, 130, 221, 20))
        self.receiver.setObjectName("receiver")
        self.mail = QtWidgets.QLabel(self.centralwidget)
        self.mail.setGeometry(QtCore.QRect(10, 130, 131, 16))
        self.mail.setObjectName("mail")
        self.signIn = QtWidgets.QPushButton(self.centralwidget)
        self.signIn.setGeometry(QtCore.QRect(350, 70, 75, 23))
        self.signIn.setObjectName("signIn")
        self.LogOut = QtWidgets.QPushButton(self.centralwidget)
        self.LogOut.setGeometry(QtCore.QRect(440, 70, 71, 23))
        self.LogOut.setObjectName("LogOut")
        self.Send = QtWidgets.QPushButton(self.centralwidget)
        self.Send.setGeometry(QtCore.QRect(420, 400, 81, 23))
        self.Send.setObjectName("Send")
        self.Title = QtWidgets.QLineEdit(self.centralwidget)
        self.Title.setGeometry(QtCore.QRect(150, 160, 221, 20))
        self.Title.setObjectName("Title")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 160, 141, 16))
        self.label_3.setObjectName("label_3")
        self.status = QtWidgets.QLabel(self.centralwidget)
        self.status.setGeometry(QtCore.QRect(50, 80, 291, 20))
        self.status.setText("")
        self.status.setObjectName("status")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(10, 80, 47, 20))
        self.label_5.setObjectName("label_5")
        self.receive = QtWidgets.QPushButton(self.centralwidget)
        self.receive.setGeometry(QtCore.QRect(180, 450, 141, 23))
        self.receive.setObjectName("receive")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(10, 450, 47, 20))
        self.label_4.setObjectName("label_4")
        self.search = QtWidgets.QLineEdit(self.centralwidget)
        self.search.setGeometry(QtCore.QRect(60, 451, 113, 21))
        self.search.setObjectName("search")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 525, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Mailapp"))
        self.sender.setText(_translate("MainWindow", " laboratorium8wno@gmail.com"))
        self.password.setText(_translate("MainWindow", "Laboratorium8WNO"))
        self.label.setText(_translate("MainWindow", "email:"))
        self.label_2.setText(_translate("MainWindow", "password:"))
        self.attachImage.setText(_translate("MainWindow", "Attach image"))
        self.img.setText(_translate("MainWindow", "0.png"))
        self.body.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Type text ...</p></body></html>"))
        self.receiver.setText(_translate("MainWindow", "laboratorium8wno@gmail.com"))
        self.mail.setText(_translate("MainWindow", "Send to/ Received from:"))
        self.signIn.setText(_translate("MainWindow", "Sign in"))
        self.LogOut.setText(_translate("MainWindow", "Log out"))
        self.Send.setText(_translate("MainWindow", "Send"))
        self.Title.setText(_translate("MainWindow", "Lab 8"))
        self.label_3.setText(_translate("MainWindow", "Subject:"))
        self.label_5.setText(_translate("MainWindow", "Status:"))
        self.receive.setText(_translate("MainWindow", "Receive mail/Next mail"))
        self.label_4.setText(_translate("MainWindow", "Subject:"))
        self.search.setText(_translate("MainWindow", "Lab 8"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
